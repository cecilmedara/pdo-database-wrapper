<?php

/*
 * The MIT License
 *
 * Copyright 2015 Cecil Medara.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of PdoCrudTest
 *
 * @author User
 */

require_once(__DIR__ . "/../PdoCrud.php");
class PdoCrudTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function Test_Set()
    {
         $pdoCrud = new PdoCrud('cec');
        $param = array(
            'table' => 'bikes',
            'data' => array(
                      'name' => 'Cecil',
                      'bike' => 'Suzuki'
                  ));
        $expected = "id = :id, name = :name";
        
        $this->assertEquals($expected, $pdoCrud->Set($param));
    }
    /**
     * @test
     */
    public function Test_Update_Set_Params()
    {
        $pdoCrud = new PdoCrud('cec');
        $param = array(
          'id' => 1,
          'name' => 'Cecil'
        );
        $expected = "id = :id, name = :name";
        
        $this->assertEquals($expected, $pdoCrud->Update_Set_Params($param));
    }
    /**
     * @test
     */
    public function Test_Add_Colons()
    {
        $pdoCrud = new PdoCrud('cec');
        $param = array(
          'bike' => 'Ducati',
          'name' => 'Cecil'
        );
        $expected = array(
            ':bike' => 'Ducati',
            ':name' => 'Cecil'
        );
        
        $this->assertEquals($expected, $pdoCrud->Add_Colons($param));
    }
}
